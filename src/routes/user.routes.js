const express = require('express');
const router = express.Router();
const User = require('../models/user.model');

/**
 * @swagger
 * /user:
 *  post:
 *      summary: Creación de un usuario
 *      tags: [User]
 #*      parameters:
 #*          -   in: path
 #*              name: email
 #*              schema:
 #*                  type: string
 #*              required: true
 #*              description: Creación de un usuario con validación de email único
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/createUser'
 *          example:
 *
 *      responses:
 *          200:
 *              description: Usuario actualizado exitosamente
 *              content:
 *                  application/json:
 *                      schema:
 *                          $ref: '#/components/schemas/createUser'
 *          500:
 *              description: no se puede actualizar el usuario
 *          404:
 *              description: usuario no encontrado
 */


router.post('/', (req, res) => {
    const newUser = req.body;
    const userInList = User.createNewUser(newUser);
    (userInList) ? res.json('Usario creado correctamente'): res.json('Ya existe un usuario con ese email, por favor intenta con otro');
});

module.exports = router;
/**
 * @swagger
 * tags:
 *  name: User
 *  description: Users section
 * components:
 *  schemas:
 *      User:
 *          type: object
 *          required:
 *              - username
 *              - name
 *              - email
 *              - pass
 *              - address
 *              - phone
 *              - country
 *              - isAdmin
 *          properties:
 *              username:
 *                  type: string
 *                  description: Nick name
 *              name:
 *                  type: string
 *                  description: Name
 *              email:
 *                  type: string
 *                  description: Email
 *              pass:
 *                  type: string
 *                  description: Password
 *              phone:
 *                  type: number
 *                  description: Phone number
 *              address:
 *                  type: string
 *                  description: Address
 *              country:
 *                  type: string
 *                  description: Country
 *              isAdmin:
 *                  type: boolean
 *                  description: admin
 *          example:
 *              username: fcndo___
 *              name: Facundo Orlando
 *              email: facu@gmail.com
 *              pass: 123456
 *              phone: 1112341234
 *              address: Gral Manuel A Rodriguez 1234
 *              country: Argentina
 *              isAdmin: false
 *      DeleteUser:
 *          type: object
 *          required:
 *              - Email
 *          properties:
 *              Email:
 *                  type: string
 *                  description: Email of the user to be deleted
 *      createUser:
 *          type: object
 *          required:
 *              - username
 *              - name
 *              - email
 *              - phone
 *              - address
 *              - country
 *          properties:
 *              username:
 *                  type: string
 *                  description: Nick name
 *              name:
 *                  type: string
 *                  description: Name
 *              email:
 *                  type: string
 *                  description: Email
 *              pass:
 *                  type: string
 *                  description: Password
 *              phone:
 *                  type: number
 *                  description: Phone number
 *              address:
 *                  type: string
 *                  description: Address
 *              country:
 *                  type: string
 *                  description: Country
 *              isAdmin:
 *                  type: boolean
 *                  description: admin
 *          example:
 *              username: fcndo___
 *              name: Facundo Orlando
 *              email: facu@gmail.com
 *              phone: 1112341234
 *              address: Gral Manuel A Rodriguez 1234
 *              country: Argentina
 */