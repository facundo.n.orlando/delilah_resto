const express = require('express');
const router = express.Router();

const adminProductRoutes = require('./admin/admin.product.routes');
const adminOrderRoutes = require('./admin/admin.order.routes');
const adminUserRoutes = require('./admin/admin.user.routes');
const adminPaymentMethodRoutes = require('./admin/admin.paymentMethod.routes');


router.use('/products', adminProductRoutes);
router.use('/orders', adminOrderRoutes);
router.use('/users', adminUserRoutes);
router.use('/paymentMethods', adminPaymentMethodRoutes);


module.exports = router;