const express = require('express');
const router = express.Router();
const Orders = require('../../models/order.model');



/**
 * @swagger
 * /admin/orders:
 *  get:
 *    summary: Ver todas las ordenes
 *    tags: [Admin order]
 *    description: Ver todas las ordenes
 *    responses:
 *      200:
 *          description: Ordenes enncontradas
 *      401:
 *          description: username and password are required
 */
router.get('/', (req, res) => {
    const orders = Orders.findAll();
    res.json(orders);
    return orders;
});


/**
 * @swagger
 * /admin/orders/{id}:
 *  get:
 *    summary: Busca una orden por su id
 *    tags: [Admin order]
 *    description: Busca una orden por su id
 *    parameters:
 *    - name: id
 *      description: id de la orden que quieres buscar
 *      in: path
 *      required: true
 *      type: number
 *    responses:
 *      200:
 *          description: orden encontrada
 *      401:
 *          description: username and password are required
 *      404:
 *          description: orden no encontrada
 */
router.get('/:id', (req, res) => {
    const { id } = req.params;
    const order = Orders.findOrderById(id);
    return order[0] ? res.status(200).json(order) : res.status(404).json(`No existe ninguna orden con id: ${id}. Intenta con otro`);

});


/**
 * @swagger
 * /admin/orders/{id}:
 *  put:
 *      summary: Actualiza datos en una orden
 *      tags: [Admin order]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: number
 *              required: true
 *              description: id de la orden a modificar
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Order'
 *      responses:
 *          200:
 *              description: Método de pago actualizada exitosamente
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          $ref: '#/components/schemas/Order'
 *          500:
 *              description: No se puede actualizar el método de pago
 *          404:
 *              description: Método de pago no encontrado
 */
router.put('/:id', (req, res) => {
    // E.G:
    // {
    // "id":500,
    // "status":"CAMBIADO"
    // }

    const { id } = req.params;
    const orderChanges = req.body;
    const changedOrder = Orders.changePropertyInOrder(id, orderChanges);
    return changedOrder ? res.status(200).json('Orden modificada') : res.status(404).json(`No se encontró un producto con el id: ${id}. Intenta con otro`);
});


/**
 * @swagger
 * /admin/orders/{id}:
 *  delete:
 *      summary: Eliminar una orden por ID
 *      tags: [Admin order]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: number
 *              required: true
 *              description: id de la orden a eliminar
 *      responses:
 *          200:
 *              description: Orden eliminada exitosamente
 *          500:
 *              description: No se pudo eliminar la orden
 *          404:
 *              description: Orden no encontrada
 */

router.delete('/:id', (req, res) => {

    // const { id } = req.params;
    // const index = Orders.findAll().findIndex(o => o.id == id);

    // Orders.findAll().splice(index, 1);
    // res.json('Delete Order');
    const { id } = req.params;
    const orderDeleted = Orders.deleteById(id);
    orderDeleted ? res.json('Orden eliminada') : res.status(404).json('No se encontró una Orden con ese ID');
});



module.exports = router;