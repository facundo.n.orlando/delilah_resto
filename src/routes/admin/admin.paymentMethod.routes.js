const express = require('express');
const router = express.Router();
const PaymentMethods = require('../../models/paymentMethod.model');

/**
 * @swagger
 * /admin/paymentMethods:
 *  get:
 *    summary: Ver todos los usuarios
 *    tags: [Admin payment-method]
 *    description: Ver todos los usuarios
 *    responses:
 *      200:
 *          description: Payment Method successfully found
 *      401:
 *          description: username and password are required
 */
router.get('/', (req, res) => {
    const paymentMethods = PaymentMethods.findAll();
    console.log("estos payments: "+paymentMethods);
    res.json(paymentMethods);
    return paymentMethods;
});

/**
 * @swagger
 * /admin/paymentMethods/{id}:
 *  get:
 *    summary: Busca un método de pago por id
 *    tags: [Admin payment-method]
 *    description: Busca un método de pago por id
 *    parameters:
 *    - name: id
 *      description: id del método de pago que buscas
 *      in: path
 *      required: true
 *      type: number
 *    responses:
 *      200:
 *          description: payment method successfully found
 *      401:
 *          description: username and password are required
 *      404:
 *          description: payment method not found
 */
router.get('/:id', (req, res) => {
    const { id } = req.params;
    const paymentMethod = PaymentMethods.findPaymentMethodById(id);
    return paymentMethod[0] ? res.status(200).json(paymentMethod) : res.status(404).json(`No existe un método de pago con id: ${id}. Intenta con otro`);
});


/**
 * @swagger
 * /admin/paymentmethods/{id}:
 *  put:
 *      summary: Actualizar datos de un método de pago
 *      tags: [Admin payment-method]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: number
 *              required: true
 *              description: Id del método de pago a modificar
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/createPaymentMethod'
 *      responses:
 *          200:
 *              description: Método de pago actualizada exitosamente
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          $ref: '#/components/schemas/createPaymentMethod'
 *          500:
 *              description: No se puede actualizar el método de pago
 *          404:
 *              description: Método de pago no encontrado
 */
router.put('/:id', (req, res) => {
    const { id } = req.params;
    const paymentMethodChanges = req.body;
    const changedPaymentMethod = PaymentMethods.changePropertyInPaymentMethod(id, paymentMethodChanges);
    res.json(changedPaymentMethod);
    return changedPaymentMethod;
});



/**
 * @swagger
 * /admin/paymentmethods:
 *  post:
 *      summary: crear nuevo método de pago
 *      tags: [Admin payment-method]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/createPaymentMethod'
 *      responses:
 *          200:
 *              description: Método de pago creado exitosamente
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          500:
 *              description: No se fue posible crear el método de pago
 *          404:
 *              description: Método de pago no encontrado
 */


router.post('/', (req, res) => {
    const newPaymentMethod = req.body;
    const paymentMethodCreated = PaymentMethods.createNewPaymentMethod(newPaymentMethod);
    (paymentMethodCreated) ? res.status(200).json('Método de pago creado correctamente') : res.json('Ya existe un método de pago con ese método y de ese banco, por favor intenta con otro o usa el buscador de métodos de pago');
});


/**
 * @swagger
 * /admin/paymentMethods/{id}:
 *  delete:
 *      summary: update user data
 *      tags: [Admin payment-method]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: number
 *              required: true
 *              description: id del método de pago a eliminar
 *      responses:
 *          200:
 *              description: Método de pago eliminado exitosamente
 *          500:
 *              description: No se pudo eliminar el método de pago
 *          404:
 *              description: Método de pago no encontrada
 */

router.delete('/:id', (req, res) => {

    const { id } = req.params;
    const paymentMethodDeleted = PaymentMethods.deleteById(id);
    paymentMethodDeleted ? res.json('Payment Method eliminado') : res.status(404).json('No se encontró un Payment Method con ese ID');
});



module.exports = router;

/**
 * @swagger
 * tags:
 *  name: Admin payment-method
 *  description: Payment Methods section
 * components:
 *  schemas:
 *      paymentMethod:
 *          type: object
 *          required:
 *              - id
 *              - method
 *              - type
 *              - bank
 *          properties:
 *              id:
 *                  type: number
 *                  description: id del método de pago
 *              method:
 *                  type: string
 *                  description: Método de pago
 *              type:
 *                  type: string
 *                  description: Tipo de método de pago
 *              bank:
 *                  type: string
 *                  description: Banco del método de pago
 *          example:
 *              id: 1
 *              method: Credito
 *              type: Tarjeta
 *              bank: Galicia
 *      createPaymentMethod:
 *          type: object
 *          required:
 *              - method
 *              - type
 *              - bank
 *          properties:
 *              method:
 *                  type: string
 *                  description: medio de pago
 *              type:
 *                  type: string
 *                  description: tipo de medio
 *              bank:
 *                  type: string
 *                  description: admin
 */
