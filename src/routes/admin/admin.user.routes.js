const express = require('express');
const router = express.Router();
const Users = require('../../models/user.model');


/**
 * @swagger
 * /admin/users:
 *  get:
 *    summary: Ver todos los usuarios
 *    tags: [Admin user]
 *    description: Ver todos los usuarios
 *    responses:
 *      200:
 *          description: users successfully found
 *      401:
 *          description: username and password are required
 */
router.get('/', (req, res) => {
    const users = Users.findAll();
    res.json(users);
    return users;
});

/**
 * @swagger
 * /admin/users/{username}:
 *  get:
 *    summary: Buscar un usuario por username
 *    tags: [Admin user]
 *    description: Busca un usuario por username
 *    parameters:
 *    - name: username
 *      description: username del usuario a buscar
 *      in: path
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *          description: user successfully found
 *      401:
 *          description: username and password are required
 *      404:
 *          description: user not found
 */
router.get('/:username', (req, res) => {
    const { username } = req.params;
    const userFound = Users.findUserByUsername(username);
    (userFound[0])?res.status(200).json(userFound):res.status(404).json(`No se ha encontrado ningún usuario con username: ${username}`);
});


/**
 * @swagger
 * /admin/users/{username}:
 *  put:
 *      summary: update user data
 *      tags: [Admin user]
 *      parameters:
 *          -   in: path
 *              name: username
 *              schema:
 *                  type: string
 *              required: true
 *              description: username of the user to be updated
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/User'
 *
 *      responses:
 *          200:
 *              description: Usuario actualizado exitosamente
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: string
 *          500:
 *              description: no se puede actualizar el usuario
 *          404:
 *              description: no se ha encontrado un usuario con ese username
 */
router.put('/:username', (req, res) => {
    // E.G:
    // {
    // "username: "pepe",
    // "address":"La Feliz 1233"
    // }

    const { username } = req.params;
    const userChanges = req.body;
    const changedUser = Users.changePropertyInUser(username, userChanges);
    return changedUser ? res.status(200).json('User modificado') : res.status(404).json(`No se encontró un user con el username: ${username}. Intenta con otro`);
});

/**
 * @swagger
 * /admin/users/{username}:
 *  delete:
 *    summary: Eliminar un usuario por username
 *    tags: [Admin user]
 *    description: Eliminar un usuario por username
 *    parameters:
 *    - name: username
 *      description: username del usuario a eliminar
 *      in: path
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *          description: user successfully deleted
 *      401:
 *          description: username and password are required
 *      405:
 *          description: invalid input
 */
router.delete('/:username', (req, res) => {
    const { username } = req.params;
    const userDeleted = Users.deleteById(username);
    userDeleted ? res.json('User eliminado') : res.status(404).json('No se encontró un User con ese username');
});

module.exports = router;
