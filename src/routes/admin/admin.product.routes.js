const express = require('express');
const router = express.Router();
const Products = require('../../models/product.model');

/**
 * @swagger
 * /admin/products:
 *  get:
 *    summary: Ver todos los productos
 *    tags: [Admin product]
 *    description: Ver todos los productos
 *    responses:
 *      200:
 *          description: Producto encontrado exitosamente
 */
 router.get('/', (req, res) => {
    const products = Products.findAll();
    res.json(products);
    return products;
});

/**
 * @swagger
 * /admin/products/{id}:
 *  get:
 *    summary: Busca un un producto por su id
 *    tags: [Admin product]
 *    description: Busca un un producto por su id
 *    parameters:
 *    - name: id
 *      description: id del método de pago que buscas
 *      in: path
 *      required: true
 *      type: number
 *    responses:
 *      200:
 *          description: producto encontrado exitosamente
 *      404:
 *          description: producto no encontrado
 */
router.get('/:id', (req, res) => {
    const { id } = req.params;
    const product = Products.findProductById(id);
    return product[0] ? res.status(200).json(product) : res.status(404).json(`No existe un método de pago con id: ${id}. Intenta con otro`);
});


/**
 * @swagger
 * /admin/products/{id}:
 *  put:
 *      summary: Actualizar datos en un producto
 *      tags: [Admin product]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: number
 *              required: true
 *              description: id del producto a modificar
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/createProduct'
 *      responses:
 *          200:
 *              description: Producto actualizada exitosamente
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          $ref: '#/components/schemas/createProduct'
 *          500:
 *              description: No se pudo actualizar el producto
 *          404:
 *              description: Producto no encontrado
 */
router.put('/:id', (req, res) => {
    // E.G:
    // {
    // "id":500,
    // "status":"CAMBIADO"
    // }

    const { id } = req.params;
    const productChanges = req.body;
    const changedProduct = Products.changePropertyInProduct(id, productChanges);
    return changedProduct ? res.status(200).json('Producto modificado') : res.status(404).json(`No se encontró un producto con el id: ${id}. Intenta con otro`);

});

// router.delete('/', (req, res) => {
//     var productsId = req.body;
//     //string ids a borrar

// var indice = arreglo.indexOf(3); // obtenemos el indice
// arreglo.splice(indice, 1); // 1 es la cantidad de elemento a eliminar

// console.log( arreglo );
//     res.json('Delete Product');
// });


/**
 * @swagger
 * /admin/products/{id}:
 *  delete:
 *      summary: Eliminar un producto por su ID
 *      tags: [Admin product]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: number
 *              required: true
 *              description: id del método de pago a eliminar
 *      responses:
 *          200:
 *              description: Producto eliminado exitosamente
 *          500:
 *              description: No se pudo eliminar el producto
 *          404:
 *              description: Producto no encontrada
 */

router.delete('/:id', (req, res) => {

    // const { id } = req.params;
    // const index = Products.findAll().findIndex(p => p.id == id);

    // Products.findAll().splice(index, 1);
    // res.json('Delete Product');

    const { id } = req.params;
    const productDeleted = Products.deleteById(id);
    productDeleted ? res.json('Producto eliminado') : res.status(404).json('No se encontró un Producto con ese ID');
});

module.exports = router;


/**
 * @swagger
 * tags:
 *  name: Admin product
 *  description: Products section
 * components:
 *  schemas:
 *      product:
 *          type: object
 *          required:
 *              - id
 *              - name
 *              - img
 *              - price
 *              - type
 *          properties:
 *              id:
 *                  type: number
 *                  description: id del producto
 *              name:
 *                  type: string
 *                  description: Nombre del producto
 *              img:
 *                  type: string
 *                  description: Imagen representativa del producto
 *              price:
 *                  type: string
 *                  description: Precio del producto
 *              type:
 *                  type: string
 *                  description: Comida o Bebida
 *          example:
 *              id: 1
 *              name: Cuarto de Libra
 *              img: https://aws.imagen.com/cuarto-de-libra
 *              price: 150
 *              type: Comida
 *      createProduct:
 *          type: object
 *          required:
 *              - name
 *              - img
 *              - price
 *              - type
 *          properties:
 *              name:
 *                  type: string
 *                  description: Nombre del producto
 *              img:
 *                  type: string
 *                  description: Imagen representativa del producto
 *              liters:
 *                  type: number
 *                  description: Litros en caso de ser una bebida
 *              price:
 *                  type: number
 *                  description: Precio del producto
 *              type:
 *                  type: string
 *                  description: Comida o Bebida
 */