const express = require('express');
const router = express.Router();
const Order = require('../models/order.model');
const isYourOrder = require('../middlewares/isYourOrder.middleware');

/**
 * @swagger
 * /orders/{id}:
 *  get:
 *    summary: Search an order by id
 *    tags: [Order]
 *    description: Busca una orden por id
 *    parameters:
 *    - name: id
 *      description: id de la orden
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *          description: order successfully found
 *      401:
 *          description: username and password are required
 *      405:
 *          description: invalid input
 */

router.get('/:id', isYourOrder, (req, res) => {
    const { id } = req.params;
    const order = Order.findOrderById(id)[0];

    (!order) ? res.status(404).json({'message':`No hemos podido encontrar la orden con id ${id}`}) : res.json(order);
    return order;
});

/**
 * @swagger
 * /orders:
 *  post:
 *      summary: Crear una orden
 *      tags: [Order]
 *      parameters:
 *         requestBody:
 *             required: true
 *             content:
 *                 application/json:
 *                     schema:
 *                         $ref: '#/components/schemas/createOrder'
 *      responses:
 *          200:
 *              description: Orden creada exitosamente
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          $ref: '#/components/schemas/createOrder'
 *          500:
 *              description: No se pudo actualizar la orden
 *          404:
 *              description: Orden no encontrado
 */

router.post('/', (req, res) => {
    Order.createNewOrder(req.body);
    res.json('Order Created');
});


/**
 * @swagger
 * /orders/{id}:
 *  put:
 *      summary: Actualizar datos en una orden
 *      tags: [Order]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: number
 *              required: true
 *              description: id de la orden a modificar
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/createOrder'
 *      responses:
 *          200:
 *              description: Orden actualizada exitosamente
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          $ref: '#/components/schemas/createOrder'
 *          500:
 *              description: No se pudo actualizar la orden
 *          404:
 *              description: Orden no encontrado
 */

router.put('/:id', isYourOrder, (req, res) => {
    const { id } = req.params;
    const orderChanges = req.body;
    const orderStatus = Order.findOrderById(id).status;
    if(orderStatus!=="open"){
        res.json(`La orden que quieres modificar ya se ha cerrado`);
        return false;
    }
    const changedOrder = Order.changePropertyInOrder(id, orderChanges);
    res.json(changedOrder);
    return changedOrder;
});


/**
 * @swagger
 * /orders/{id}:
 *  delete:
 *      summary: Eliminar orden
 *      tags: [Order]
 *      parameters:
 *          -   in: path
 *              name: id
 *              schema:
 *                  type: number
 *              required: true
 *              description: Id de la orden a eliminar
 *      responses:
 *          200:
 *              description: Orden eliminada exitosamente
 *          500:
 *              description: No se pudo eliminar la orden
 *          404:
 *              description: Orden no encontrada
 */
router.delete('/:id', isYourOrder, (req, res) => {
    const { id } = req.params;
    const orderDeleted = Order.deleteById(id);
    orderDeleted ? res.status(200).json('Orden eliminada') : res.status(404).json('No se encontró una Orden con ese ID');
});


module.exports = router;


/**
 * @swagger
 * tags:
 *  name: Order
 *  description: Orders section
 * components:
 *  schemas:
 *      Order:
 *          type: object
 *          required:
 *              - id
 *              - owner
 *              - orderedProducts
 *              - status
 *              - address
 *              - comment
 *              - created_at
 *              - updated_at
 *          properties:
 *              id:
 *                  type: string
 *                  description: id de la orden
 *              owner:
 *                  type: string
 *                  description: username del owner
 *              orderedProducts:
 *                  type: array
 *                  description: array con productos
 *              status:
 *                  type: string
 *                  description: estado de la orden
 *              address:
 *                  type: string
 *                  description: dirección de destino
 *              comment:
 *                  type: string
 *                  description: comentario para el restaurante
 *          example:
 *              id: 1
 *              owner: fcnd___
 *              orderedProducts: [{...},{...}]
 *              status: open
 *              address: Gral Manuel A Rodriguez 1234
 *              comment: "Que por favor me mande mensaje cuando llegue"
 *      deleteOrder:
 *          type: object
 *          required:
 *              - id
 *          properties:
 *              id:
 *                  type: number
 *                  description: id de la orden a eliminar
 *      createOrder:
 *          type: object
 *          required:
 *              - orderedProducts
 *              - address
 *              - comment
 *              - created_at
 *              - updated_at
 *          properties:
 *              orderedProducts:
 *                  type: array
 *                  description: array con productos
 *              address:
 *                  type: string
 *                  description: dirección de destino
 *              comment:
 *                  type: string
 *                  description: comentario para el restaurante
 *          example:
 *              orderedProducts: [{...},{...}]
 *              address: Gral Manuel A Rodriguez 1234
 *              comment: "Que por favor me mande mensaje cuando llegue"
 */