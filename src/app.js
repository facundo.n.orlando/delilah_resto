// initialization
const express = require('express');
const basicAuth = require('express-basic-auth');
require('dotenv').config();

// own modules
const userRoutes = require('./routes/user.routes');
const orderRoutes = require('./routes/order.routes');
const adminRoutes = require('./routes/admin.routes');

// middlewares
const myCustomAuthorizer = require('./middlewares/basic-auth.middleware');
const isAdmin = require('./middlewares/isAdmin.middleware');

// swagger documentation
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const swaggerOptions = require('./utils/swaggerOptions');

const app = express();
const PORT = process.env.PORT || 3005

app.use(express.json());
app.use((req, res, next) => {
    console.log('Time: ', Date(Date.now()));
    next();
});

// Documentacion swagger
const swaggerSpecs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

app.use(basicAuth({
    authorizer: myCustomAuthorizer
}));
app.use('/orders', orderRoutes);
app.use('/admin', isAdmin, adminRoutes);
app.use('/user', userRoutes);
app.listen(PORT, () => {
    console.log("server listing on port", PORT);
});
/**
 * @swagger
 * paths;
 *  /user:
 *   get:
 *       summary: Return the list of users
 *       tags: [Users]
 *       responses:
 *           200:
 *               description: The list of users
 *               content: 
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/User'
 *           401:
 *               description: username and password are required
 */