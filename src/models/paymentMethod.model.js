const paymentMethods = [
    {
        id:1,
        method: "Crédito",
        type: "Tarjeta",
        bank: "Galicia",
        "created_at": 1630790588881
    },
    {
        id:2,
        method: "Débito",
        type: "Tarjeta",
        bank: "Galicia",
        created_at: 1630790588882
    },
    {
        id:3,
        method: "Efectivo",
        type: "Efectivo",
        bank: false,
        created_at: 1630790588883
    }
];

const findAll = () => {
    return paymentMethods;
}
const findPaymentMethodById = (id) => {
    return paymentMethods.filter(pm => pm.id == id);
}
const findPaymentMethodsByType = (type) => {
    return paymentMethods.filter(pm => pm.type == type);
}

const findPaymentMethodByMethodAndBank = (method,bank) => {
    const methodsByBank = paymentMethods.filter(pm => pm.bank == bank);
    if(methodsByBank.length>0){
        const methodsFilteredByMethod = methodsByBank.filter(pm => pm.method == method);
        return (methodsFilteredByMethod.length>0)? methodsFilteredByMethod: false;
    }
    return false;
}
const createNewPaymentMethod = (newPaymentMethod) => {
    const alreadyExistPaymentMethod = findPaymentMethodByMethodAndBank(newPaymentMethod.method, newPaymentMethod.bank);
    if(alreadyExistPaymentMethod==false){
        let idMax = 0;

        for ( let paymentMethod of paymentMethods ) {
            if (idMax < paymentMethod.id){
                idMax = paymentMethod.id
            }
        }
        newPaymentMethod.id = idMax + 1;
        newPaymentMethod.created_at = Date.now();
        return paymentMethods.push(newPaymentMethod);
    }
    return false;
}

const changePropertyInPaymentMethod = (id, changesObj) => {
    const paymentMethod = findPaymentMethodById(id)[0];
    for(const property in changesObj){
        if(!paymentMethod[property]){
            return false; //sale del if o del for o del put?
        }
        paymentMethod[property] = changesObj[property];
    }
    return paymentMethod;
}

const deleteById = (id) => {
    const index = paymentMethods.findIndex(pm => pm.id == id);
    if(index>=0){
        paymentMethods.splice(index, 1);
        return true;
    }
    return false;
}


module.exports = { findAll, findPaymentMethodById, findPaymentMethodsByType, createNewPaymentMethod, changePropertyInPaymentMethod, deleteById }