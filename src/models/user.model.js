const users = [
    {
        username: "admin",
        name: "Admin I Strator",
        email: "admin@gmail.com",
        password: "admin",
        phone: "1143218765",
        address: "Gral Paz 6789",
        country: "Argentina",
        isAdmin: true
    },
    {
        username: "user",
        name: "Facundo Orlando",
        email: "facu@gmail.com",
        password: "pass",
        phone: "1112345678",
        address: "Gral Pepe Argento 1332",
        country: "Argentina",
        isAdmin: false
    }
];

const findAll = () => {
    return users;
}
const findUserByUsername = (username) => {
    const userFound = users.filter(user => user.username == username);
    if(!userFound){
        return false;
    }
    return userFound;
}
const findUserByEmail = (email) => {
    return users.filter(user => user.email == email);
}
const createNewUser = (user) => {
    const alreadyExistEmailInUser = findUserByEmail(user.email);

    // PROBÉ CREAR USUARIO PERO SIN MANDAR EMAIL O SI, SALTA QUE YA EXISTE, DEBEO PROBAR ESTO AVER COMO ESTA FUNCIONANDO
    if(alreadyExistEmailInUser==false){
        users.push(user);
        return true;
    }
    return false;
}

const changePropertyInUser = (username, changesObj) => {
    const user = findUserByUsername(username)[0];

    if(user){
        for(const property in changesObj){
            if(!user[property]){
                console.log(`La propiedad ${property} no existe`);
            }
            console.log(`Se va a cambiar el valor de la propiedad: ${property}, de ${user[property]} a ${changesObj[property]} `);
            user[property] = changesObj[property];
        }
        return user;
    }
    return false;
}

const deleteById = (username) => {
    const index = users.findIndex(u => u.username == username);
    if(index>=0){
        users.splice(index, 1);
        return true;
    }
    return false;
}

module.exports = { findAll, findUserByUsername, findUserByEmail, createNewUser, changePropertyInUser, deleteById }