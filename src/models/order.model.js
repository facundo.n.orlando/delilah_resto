// status: [pending, confirmed, in-progress, sent, delivered]

const orders = [
    {
        id:1,
        owner: "admin",
        orderedProducts: [{ id:1, amount:4}, {id:6,amount:1}],
        status: "open",
        address:"Gral Paz 112",
        comment: "Es sorpresa, por favor no toques timbre! Mandame un mensaje",
        created_at:1522051265094,
        updated_at:1622053875094
    },
    {
        id:2,
        owner: "user",
        orderedProducts: [{ id:1, amount:2}, {id:7,amount:2}],
        status: "sent",
        address:"Gral Manuel A. Rodriguez 992",
        comment: "",
        created_at:1622051265094,
        updated_at:1622053865094
    },
    {
        id:3,
        owner: "user",
        orderedProducts: [{ id:4, amount:1}, { id:7, amount:1 }],
        status: "delivered",
        address:"Av. Libertador 1992",
        comment: "",
        created_at:1622051965094,
        updated_at:1622051865094
    },
    {
        id:4,
        owner: "user",
        orderedProducts: [{ id:2, amount:1}, { id:2, amount:1 }],
        status: "open",
        address:"Gral Manuel A. Rodriguez 9999",
        comment: "",
        created_at:1622051265094,
        updated_at:1622053865094
    }
];

const findAll = () => {
    return orders;
}
const findOrderById = (id) => {
    return orders.filter(order => order.id == id);
}
const findOrdersByUsername = (username) => {
    const history = orders.filter(o => o.owner == username && o.status !== "open"); //> [{…}, {…}, {…}]
    const openOrder = orders.filter(o => o.owner == username && o.status == "open");
    return {'openOrder': openOrder[0], 'history': history}; //si están vacías no existen ordenes, el front muestra msj
}
const createNewOrder = (order) => {
    // > order = {
    //     owner: "user",
    //     orderedProducts: [{ id:4, amount:1}],
    //     status: "confirmed",
    //     address:"Av. Libertador 1992",
    //     comment: ""
    // }
    let idMax = 0;

    for ( let order of orders ) {
        if (idMax < order.id){ idMax = order.id }
    }
    order.id = idMax + 1;
    order.created_at = Date.now();
    orders.push(order);
}

const addProductsInOrder = (ownerUsername, products) => {
    // > products = {id: products.id, amount: products.amount}
    const order = orders.filter(o => o.owner == ownerUsername)[0];
    if(order.status !== "open"){
        console.log("La orden ya se ha confirmado");
    }
    order.orderedProducts.push(products);
    order.updated_at = Date.now();

    console.log(`Se ha agregado un producto nuevo a la orden de ${ownerUsername}`);
}

const changePropertyInOrder = (id, changesObj ) => {
    const order = findOrderById(id)[0];
    if(order){
        for(const property in changesObj){
            if(!order[property]){
                console.log(`La propiedad ${property} no existe`);
                return false; //sale del if o del for o del put?
            }
            if(property==="address" || property === "orderedProducts" || property === "comment"){
                order[property] = changesObj[property];
                console.log(`Se ha cambiado el valor de la propiedad: ${property}, pasó de: ${order[property]} a: ${changesObj[property]}  `)
            }
            console.log(`La propiedad: ${property}, solo puede ser cambiada por admins`);
        }
        return order;
    }
    return false;
}

const deleteById = (id) => {
    const index = orders.findIndex(pm => pm.id == id);
    if(index>=0){
        orders.splice(index, 1);
        return true;
    }
    return false;
}

module.exports = { findAll, findOrderById, findOrdersByUsername, createNewOrder, addProductsInOrder, changePropertyInOrder, deleteById}