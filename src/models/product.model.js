const products = [
    {
        id:1,
        name: "Choripan Argentino",
        img: "productImageLink",
        price: "150",
        type: "comida"
    },
    {
        id:2,
        name: "Pancho Dotto",
        img: "productImageLink",
        price: "90",
        type: "comida"
    },
    {
        id:3,
        name: "Doble cuarto de libra",
        img: "productImageLink",
        price: "150",
        type: "comida"
    },
    {
        id:4,
        name: "Bigmac Argentino",
        img: "linkOfProduct",
        price: "350",
        type: "comida"
    },
    {
        id:5,
        name: "McNifica",
        img: "linkOfProduct",
        price: "280",
        type: "comida"
    },
    {
        id:6,
        name: "Coca Cola",
        liters: "1",
        img: "linkOfProduct",
        price: "80",
        type: "bebida"
    },
    {
        id:7,
        name: "Coca Cola",
        liters: "0.6",
        img: "linkOfProduct",
        price: "50",
        type: "bebida"
    }
];


const findAll = () => {
    return products;
}
const findProductById = (id) => {
    return products.filter(product => product.id == id);
}
const createNewProduct = (product) => {
    products.push(product);
}

const changePropertyInProduct = (id, changesObj) => {
    const product = findProductById(id)[0];
    if(product){
        for(const property in changesObj){
            if(!product[property]){
                console.log(`La propiedad ${property} no existe`);
            }
            product[property] = changesObj[property];
            console.log(`Se ha cambiado el valor de la propiedad: ${property}, de ${product[property]} a ${changesObj[property]}  `)
        }
        return product;
    }
    return false;
}
const deleteById = (id) => {
    const index = products.findIndex(pm => pm.id == id);
    if(index>=0){
        products.splice(index, 1);
        return true;
    }
    return false;
}

module.exports = { findAll, findProductById, createNewProduct, changePropertyInProduct, deleteById }