const basicAuth = require('express-basic-auth');
const User = require('../models/user.model');

function myCustomAuthorizer(username, password) {

    const user = User.findUserByUsername(username);

    if (user.length <= 0) return false;

    const userMatches = basicAuth.safeCompare(username, user[0].username);
    const passwordMatches = basicAuth.safeCompare(password, user[0].password);

    return userMatches & passwordMatches;
}

module.exports = myCustomAuthorizer;