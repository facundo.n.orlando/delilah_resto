const Users = require('../models/user.model');

function isAdmin(req, res, next) {
    const user = Users.findUserByUsername(req.auth.user)[0];
    (user.isAdmin) ? next() : res.status(401).json("Unauthorized - Not an admin");
}

module.exports = isAdmin;