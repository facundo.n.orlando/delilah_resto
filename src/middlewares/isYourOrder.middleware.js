const Orders = require('../models/order.model');

function isYourOrder(req, res, next) {
    const { id } = req.params;
    if(id){
        const order = Orders.findOrderById(id)[0];
        return (order.owner === req.auth.user) ? next() : res.status(403).json(`La orden con id: ${id} no te pertenece`);
    }
    return res.status(403).json("El id que ingresaste no es válido");
}

module.exports = isYourOrder;