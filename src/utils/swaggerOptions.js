const swaggerOptions = {
    definition: {
        openapi: "3.0.1",
        info: {
            title: "Delilah Resto API",
            version: "1.0.0",
            description: "En esta documentación vamos a poder encontrar las acciones posibles de ralizar por el administrador y el usuario"
        },
        servers: [
            {
                url: 'http://localhost:3005',
                description: "Local server"
            }
            // ,{
            //     url: 'http://www.testing-stage.delilahresto.net:3000',
            //     description: "Dev server"
            // }
        ],
        components: {
            securitySchemes: {
                basicAuth: {
                    type: "http",
                    scheme: "basic"
                }
            }
        },
        security: [
            {
                basicAuth: []
            }
        ]
    },
    apis: ["./src/routes/*.js", "./src/routes/admin/*.js"]
}

module.exports = swaggerOptions;