# Instalación
>Debes tener **instalado GIT y NPM** en tu computadora para poder hacer uso del proyecto

## Clonación del proyecto
1. Copia el siguiente comando: `$ git clone https://gitlab.com/facundo.n.orlando/delilah_resto.git`
2. Ve a tu Editor de texto > abre la consola  y posicionate en la carpeta donde quieres descargar el proyecto _(puedes usar el comando CD para moverte entre las carpetas)_
4. Pega el comando del paso 1.

## Uso
1. Posicionate dentro de la carpeta del proyecto _(delilah_resto)_
2. Tipea el comando: `$ npm i`
3. Tipea el comando: `$ npm start`
4. Ingresa a tu navegador y ve a la dirección: **localhost:3005/api-docs** para hacer uso de la UI que provee [Swagger](https://swagger.io/)
5. Reutiliza este código para cualquier aplicación que tengas pensado
