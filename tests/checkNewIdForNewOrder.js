// Recorro las ordenes ya existentes y chequeo el id más alto para sumarle 1 y poner la orden entrante con ese id máximo + 1
const orders = [{id:1}, {id:23}, {id:2}, {id:77}, {id:5}];

let idMax = 0;

for ( let order of orders ) {

  if (idMax < order.id)
    idMax = order.id;
}

console.log(idMax);
