// status: [pending, confirmed, in-progress, sent, delivered]

const orders = [
    {
        id:1,
        owner: "username",
        orderedProducts: [{ id:1, amount:4}, {id:6,amount:1}],
        status: "in-progress",
        address:"Gral Paz 112",
        comment: "Es sorpresa, por favor no toques timbre! Mandame un mensaje"
    },
    {
        id:2,
        owner: "facu",
        orderedProducts: [{ id:1, amount:2}, {id:7,amount:2}],
        status: "sent",
        address:"Gral Manuel A. Rodriguez 992",
        comment: ""
    },
    {
        id:3,
        owner: "admin",
        orderedProducts: [{ id:6, amount:1}, {id:3,amount:1}],
        status: "confirmed",
        address:"Av. Libertador 1992",
        comment: ""
    },{
        id:4,
        owner: "admin",
        orderedProducts: [{ id:1, amount:1}, {id:2,amount:1}],
        status: "open",
        address:"Av. Libertador 1992",
        comment: ""
    }
    
];

function filtrarOpen(username){
    const openOrder = orders.filter(o => o.owner == username && o.status == "open");
    const history = orders.filter(o => o.owner == username && o.status !== "open"); // [{…}, {…}, {…}]
}